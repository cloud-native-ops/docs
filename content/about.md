---
title: About Cloud Native Operations
subtitle: I thought we spelled Cloud with a K now ?
comments: false
---
Cloud Native Operations is nothing new, we are not inventing anything here,
rather we're taking learnings from 10 years of devops from SRE practices,
etc. Cloud Native Operations is not a set of prescribed practices or tools,
rather its a set of ideals and goals that lead towards resilient systems.

Cloud Native Operations is the acknowledgement that most applications should
be run on some form of Cloud Platform, and the concepts remain roughly the
same whether that platform be an IaaS, CaaS, PaaS, or other. A Cloud Native
Operator builds on top of the platform adding value and reducing toil.

Cloud Native Operations is automating as much of the entire deployment pipeline
of an application as sensible, with the specific goal of reducing (if not
removing altogether) the human toil involved in doing so.

A Cloud Native Operator writes code to do as much of their job as possible,
they string together a build and deployment pipeline using declarative orchestration and automation tools to ensure that an application can go from Developer Laptop to running in production with zero human touch.

Cloud Native Operators do not suffer from Not Invented Here syndrome, rather they embrace platforms like Kubernetes and hosted systems such as Github/Gitlab, to CI systems like Travis CI and Circle CI. If somebody else is an expert at running a system and provides a simple and reliable way to utilize that system a Cloud Native Operator's first choice is to use that system.

However there are times where for any number of reasons a hosted system cannot be used, for many reasons from data sovereignty, to organizational decisions outside of the operator's control. The Cloud Native Operator doesn't rail against this, but instead uses the same techniques to run these systems themselves as the applications that will be deployed on them. The Cloud Native Operator holds strong opinions, but pragmatic indifference, they do not die on hills, rather they work within the confines that exist.