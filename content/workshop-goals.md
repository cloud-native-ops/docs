---
title: Cloud Native Operations Workshop Goals
comments: false
---

It’s clear that the future of deploying software is all about automation and strong feedback loops. High functioning teams are doing CI/CD to get from development to production in hours rather than months. A typical workflow looks something like this.

![Example Cloud Native Operations Pipeline](/images/cloud-native-ops-pipeline.png)

Notice there are a lot of stages listed, Source Control, testing, artifact storage, delivery, and a cloud platform.  Where and how are these tools run? This is where this workshop will focus.

The high-level concept is to show how we can use a modern platform like Kubernetes as the underpinning infrastructure for all of this. Starting with Kubernetes as the base we will explore various topics of what makes good cloud native operations (devops anyone?) and demonstrate them using opensource tools running on Kubernetes as examples.

# Agenda

## Introduction to the day

## Kubernetes

* Kubernetes 101
* how it enables the future of operations
* deployments/services/statefulsets/operators/helm
* Extend with ingress controller, external dns controller, cert-mananger/letsencrypt etc

## Version Control

* Deploy Gitlab to kubernetes using helm and putting into practice the discussions from above.
* Use certmanager/dns to set up access.
* As its installing talk through gitlab infra, basic git and gitops concepts
* Talk about the infrastructure that helm builds out and how its “prod grade”
* Push “application”  (book info or something dumb?)  to git repo.
* dockerfile
* Demo locally
* etc

## Continuous Integration

Now we’ve got code … we need to test it and build a deployable artifact.

* Use Helm to deploy CI system of choice
* Talk through infra/deployment and how its “prod grade”
* Integrate CI with gitlab repo

## Artifact Storage

Need somewhere to store the deployable artifact!

* Deploy harbor using helm ( or show how its deployed alongside PKS with opsman )
* Talk about harbor / image scanning / signing / etc. and how its “prod” deployed.
* Hook CI up to push the artifact into harbor

## Continuous Delivery

Talk about need to for CD, give intro to spinnaker and its multi-cloud inventory thing

### Deploy spinnaker with helm

* Talk through how pipelines work, etc
* Talk through how its installed and is “prod”
* Create pipeline to take newly built artifact in harbor and deploy to Kubernetes
* Build a full pipeline with spinnaker to deploy to dev, run tests, deploy to prod.

## Observability

Great now we have it … lets set up monitoring/logging/alerting… again modern shit here.  Maybe bring in datadog/pagerduty/wavefront/etc

## Summary

Remind everyone of what we just achieved.  We built a modern application and deployed it to modern infrastructure using modern techniques ( omg devops amirite)

## Red Team Happy hour

Unleash the chaos!
Do some bad things to the various levels of infra/apps and show how it auto-heals.
